/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allfordeal.gui;

import com.allfordeal.entities.Service;
import com.allfordeal.handler.ServiceHandler;
import java.io.DataInputStream;
import java.io.IOException;
import java.rmi.ServerException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author fatouh
 */
public class FormAfficheService extends Form implements CommandListener, Runnable{

    Service [] services;
    StringBuffer sb;
    public FormAfficheService(String title) {
        super(title);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        
    }

    public void run() {
               try {
          
            ServiceHandler serviceHandler = new ServiceHandler();
            // get a parser object
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/sprintMobile/allfordeal/select.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            parser.parse(dis, serviceHandler);
            // display the result
            services = serviceHandler.getService();

            if (services.length > 0) {
                for (int i = 0; i < services.length; i++) {
                    String v;
                    if(services[i].isValidation())
                        v="valid";
                    else v="non valid";
                   
                    String resultat ="nom: "+services[i].getNom() +" categorie: "+ services[i].getCategorie()+"\ndescription: "+services[i].getDescription()
                           +"\n validation: "+ v +"\n nbpoints: "+services[i].getNbPoint()+"\ndifficulte: "+services[i].getDifficulte();
                    append(resultat);
                }
            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
    }

    

