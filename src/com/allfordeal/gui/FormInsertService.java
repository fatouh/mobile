/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allfordeal.gui;

import allfordeal.Midlet;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author fatouh
 */
public class FormInsertService extends Form implements CommandListener {

    
    Command cmdNext = new Command("Add", Command.OK, 0);
    Command cmdback = new Command("Back", Command.OK, 1);
    TextField tfNom = new TextField("Nom:", "", 30, TextField.ANY);
    TextField tfCategorie = new TextField("Categorie:", "", 30, TextField.ANY);
    TextField tfDesc = new TextField("Description:", "", 100, TextField.ANY);
    ChoiceGroup tfdiff = new ChoiceGroup("Difficulte:", ChoiceGroup.POPUP);
    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb ;
    
      String url="http://localhost/sprintMobile/allfordeal/insert.php?";
    public FormInsertService(String title) {
        super(title);
        addCommand(cmdNext);
        addCommand(cmdback);
        setCommandListener(this);
        append(tfNom);
        append(tfCategorie);
        append(tfDesc);
        append(tfdiff);
        tfdiff.append("facile", null);
        tfdiff.append("moyen", null);
         tfdiff.append("difficile", null);
    }

    public void commandAction(Command c, Displayable d) {
       if(c== cmdback){
          Midlet.mMidlet.disp.setCurrent(new formAccueil("Hello"));
       }
       if(c== cmdNext){
                  try {
            String nom,categorie,description,difficulte;
            nom= tfNom.getString();
            categorie = tfCategorie.getString();
            description = tfDesc.getString();
            difficulte = tfdiff.getString(tfdiff.getSelectedIndex());
            hc=(HttpConnection)Connector.
                    open(url+"n="+nom+"&c="+categorie+"&d="+description+"&dif="+difficulte+"&m="+5);
       dis=hc.openDataInputStream();
           
       int ascii;
       sb =new  StringBuffer();
       
       while( (ascii=dis.read()) != -1 ){
           
           sb.append((char)ascii);
           
       }
       
       
       if(sb.toString().equals("successfully added")){
           Alert a= new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
           a.setTimeout(3000);
           Midlet.mMidlet.disp.setCurrent(a);
       }else{
           Alert a= new Alert("Information", sb.toString(), null, AlertType.ERROR);
           a.setTimeout(3000);
          Midlet.mMidlet.disp.setCurrent(a);
       }
       
      
        
        } catch (IOException ex) {
            ex.printStackTrace();
        }
       }
       
    }
    
}
