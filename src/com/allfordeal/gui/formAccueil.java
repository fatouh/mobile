/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allfordeal.gui;

import allfordeal.Midlet;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

/**
 *
 * @author fatouh
 */
public class formAccueil extends Form implements CommandListener {
 Command cmdadd = new Command("Add Service", Command.OK, 1);
 Command cmdaffiche = new Command("Mes Service", Command.OK, 1);
 Command cmdExit = new Command("Exit", Command.EXIT, 0);
    public formAccueil(String title) {
        super(title);
         addCommand(cmdadd);
         addCommand(cmdaffiche);
         addCommand(cmdExit);
         setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
         if (c==cmdExit){
           Midlet.mMidlet.notifyDestroyed();
       }
       if (c==cmdadd){
           Midlet.mMidlet.disp.setCurrent(new FormInsertService("New Service"));
       }
       if (c==cmdaffiche){
           Midlet.mMidlet.disp.setCurrent(new FormAfficheService("Mes Service"));
       }
    }
    
}
