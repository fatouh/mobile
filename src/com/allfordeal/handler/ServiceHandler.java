/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allfordeal.handler;

import com.allfordeal.entities.Service;
import com.allfordeal.entities.Utilisateur;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author fatouh
 */
public class ServiceHandler extends DefaultHandler{
     private Vector serviceVector;
 
    public ServiceHandler() {
        serviceVector = new Vector();
    }
     public Service[] getService() {
        Service[] personTab = new Service[serviceVector.size()];
        serviceVector.copyInto(personTab);
        return personTab;
    }
     private Service currentService;
     
      public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("service")) {
		    // create new Person object
            
            // "attributes" holds name and value pairs from inside the tag
            String id = attributes.getValue("idService");
            
            String nom = attributes.getValue("nom");
            String categorie = attributes.getValue("categorie");
            String description = attributes.getValue("description");
           String difficulte = attributes.getValue("difficulte");
            String validation = attributes.getValue("validation");
            String nbpoints = attributes.getValue("nbpoints");
            boolean b = validation.equals("1");
           
            currentService = new Service(Integer.parseInt(id),nom,categorie,description, b, Integer.parseInt(nbpoints), difficulte);
            
            
        }
    }
      public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("service")) {
            // add completed Person object to collection
            serviceVector.addElement(currentService);
            
            // we are no longer processing a <person.../> tag
            currentService = null;
        
        }
    }
}
